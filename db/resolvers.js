const Usuario = require('../models/usuarios');
const Producto = require('../models/productos');
const Cliente = require('../models/cliente');
const Pedido = require('../models/pedido');
const bcryptjs = require('bcryptjs');
require('dotenv').config({path:'variables.env'});
const jwt = require('jsonwebtoken');
const pedido = require('../models/pedido');



const crearToken=(usuario, palabraSecreta, expiresIn)=>{
    const{id, email, nombre, apellido}=usuario;
    return jwt.sign({id,email,nombre,apellido},palabraSecreta, {expiresIn});
}

const resolvers={
    Query:{
        obtenerUsuario: async(_,{}, ctx) =>{
            
            return ctx.usuario;
        },
        obtenerProductos:async()=>{
            try {
                const productos =await Producto.find({});
                return productos;
            } catch (error) {
                console.log(error);
            }
        },
        obtenerProducto:async(_,{id})=>{
                const producto = await Producto.findById(id);
                if(!producto){
                    throw new Error('El producto no existe');
                }
                return producto;
        },
        obtenerClientes:async()=>{
            try{
                const clientes = await Cliente.find({});
                return clientes;
            }catch (error){
                console.log(error);

            }
        },

        obtenerClientesVendedor:async(_,{},ctx)=>{
            try {
                const clientes = await Cliente.find({vendedor: ctx.usuario.id.toString()});
                return clientes;
            } catch (error) {
                console.log(error)
            }
        },
        obtenerCliente:async(_,{id})=>{
            try {
                const cliente = await Cliente.findById(id);
                if(!cliente){
                    throw new Error('El cliente no existe');
                }
                if(cliente.vendedor.toString() !== ctx.usuario.id){
                    throw new Error('No tienes las credenciales para ver este cliente');
                }
                return cliente;
            } catch (error) {
                console.log(error)
            }
        },
        obtenerPedidos:async()=>{
            try {
                const pedidos = Pedido.find({});
                return pedidos;
            } catch (error) {
                console.log(error)
            }
        },
        obtenerPedidosVendedor:async(_,{},ctx)=>{
            try {
                const pedidos = await Pedido.find({vendedor:ctx.usuario.id});
                return pedidos;
            } catch (error) {
                console.log(error);
            }
        },
        obtenerPedido:async(_,{id},ctx)=>{
            //Verificar si existe
            const pedido = await Pedido.findById(id);
            if(!pedido){
                throw new Error('Pedido no encontrado');
            }
            //quien lo creo puede verlo
            if(pedido.vendedor.toString() !== ctx.usuario.id){
                throw new Error('Acción no permitida');
            }
            //retornar el resultado
            return pedido;
        },
        obtenerPedidosEstado:async(_,{estado},ctx)=>{
            const pedidos = await Pedido.find({vendedor:ctx.usuario.id, estado});
            return pedidos;
        },
        mejoresClientes:async()=>{
            const clientes = await Pedido.aggregate([
                {$match:{estado:"COMPLETADO"}},
                {$group:{
                    _id:"$cliente",
                    total:{$sum:'$total'}
                }},
                {
                    $lookup:{
                        from:'clientes',
                        localField:'_id',
                        foreignField:'_id',
                        as:'cliente'
                    }
                },
                {
                    $sort:{total:-1}
                }
            ]);
            return clientes;
        },
        mejoresVendedores:async()=>{
            const vendedores = await Pedido.aggregate([
                {$match: {estado:"COMPLETADO"}},
                {$group: {
                    _id:"$vendedor",
                    total:{$sum: 'total'}
                }},
                {
                    $lookup:{
                        from:'usuarios',
                        localField: '_id',
                        foreignField:'_id',
                        as:'vendedor'
                    }
                },
                {
                    $limit:3
                },
                {
                    $sort:{total:-1}
                }
            ])
        },
        buscarProducto:async(_,{texto})=>{
            const productos = await Producto.find({$text: {$search: texto}}).limit(10)
            return productos
        }

    },
    Mutation:{
        nuevoUsuario: async (_,{input}) =>{
            //Revisar si el usuario ya está registrado
            const {email, password} = input;
            const existeUsuario = await Usuario.findOne({email});
            if(existeUsuario){
                throw new Error('El usuario ya está registrado');
            }

            //Hashear la password
            const salt = await bcryptjs.genSalt(10);
            input.password = await bcryptjs.hash(password, salt);


            //Guardar Base de datos
            try {
                const usuario = new Usuario(input);
                usuario.save();
                return usuario;
            } catch (error) {
                console.log(error);
            }

        },

        autenticarUsuario: async(_, {input})=>{
            //Revisar si el usuario existe
            const {email, password} = input;
            const existeUsuario = await Usuario.findOne({email});
            if(!existeUsuario){
                throw new Error('El usuario no está registrado');
            }

            //Revisar si el password es correcto
            const passwordCorrecto = await bcryptjs.compare(password, existeUsuario.password);
            if(!passwordCorrecto){
                throw new Error('El password es incorrecto');
            }

            //Crear el token

            return{
                token:crearToken(existeUsuario,process.env.SECRETA, '24h')
            }
        },

        nuevoProducto:async(_,{input})=>{
            try {
                const producto = new Producto(input);
                const resultado = await producto.save();
                return resultado;
            } catch (error) {
                console.log(error);
            }
        },
        actualizarProducto:async(_,{id,input})=>{
            let producto = await Producto.findById(id);
            if(!producto){
                throw new Error('El producto no existe');
            }
            producto = await Producto.findOneAndUpdate({_id:id}, input, {new:true});
            return producto;
        },
        eliminarProducto:async(_,{id})=>{
            const producto = await Producto.findById(id);
            if(!producto){
                throw new Error('El producto no existe');
            }
            await Producto.findOneAndDelete({_id:id});
            return 'Producto eliminado'

        },

        nuevoCliente:async(_,{input}, ctx)=>{

            const {email}=input
            //Verificar si el cliente esta registrado
            const cliente = await Cliente.findOne({email});
            if(cliente){
                throw new Error('El cliente ya está registrado');
            }
            try {
                console.log('Context',ctx)
                const nuevoCliente = new Cliente(input);
                //asignar el vendedor
                nuevoCliente.vendedor = ctx.usuario.id;
                //Guardar el cliente
                const resultado = await nuevoCliente.save();
                return resultado;
            } catch (error) {
                console.log(error);
            }
        },
        actualizarCliente:async(_,{id,input},ctx)=>{
            //Verificar si existe el cliente
            let cliente = await Cliente.findById(id)
            if(!cliente){
                throw new Error('Ese cliente no existe');
            }
            //Verificar si el vendedor es el asignado
            if(cliente.vendedor.toString() !== ctx.usuario.id){
                throw new Error('No tienes permisos para editar este cliente')
            }
            //Guardar cliente
            cliente = await Cliente.findOneAndUpdate({_id: id}, input, {new:true});
            return cliente;

        },
        eliminarCliente:async(_,{id},ctx)=>{
            let cliente = await Cliente.findById(id)
            if(!cliente){
                throw new Error('Ese cliente no existe');
            }
            //Verificar si el vendedor es el asignado
            if(cliente.vendedor.toString() !== ctx.usuario.id){
                throw new Error('No tienes permisos para editar este cliente')
            }
            //Eliminar cliente
            try {
                cliente = await Cliente.findOneAndDelete({_id: id});
                return 'El cliente fue eliminado';
            } catch (error) {
                console.log(error)
            }
        },
        nuevoPedido:async(_,{input},ctx)=>{
            //Verificar si el cliente existe o no
            const {cliente}=input;
            let clienteExiste = await Cliente.findById(cliente);
            if(!clienteExiste){
                throw new Error('El cliente no existe');
            }
            //Verificar si el cliente es del vendedor
            if(clienteExiste.vendedor.toString()!==ctx.usuario.id){
                throw new Error('No tienes permiso para hacer esto');
            }
            //Revisar que el stock este disponible
            for await(const articulo of input.pedido){
                const {id} =articulo;
                const producto = await Producto.findById(id);
                if(articulo.cantidad > producto.existencia){
                    throw new Error(`El producto ${producto.nombre} excede la cantidad registrada`)
                }else{
                    //Restar
                    producto.existencia = producto.existencia - articulo.cantidad
                    await producto.save();
                }

            }
            // Crear un nuevo pedido
            const nuevoPedido = new Pedido(input);
            //asignar un vendedor
            nuevoPedido.vendedor= ctx.usuario.id;
            //Guardar en ela base de datos
            const resultado = await nuevoPedido.save();
            return resultado;
        },
        actualizarPedido:async(_,{id, input},ctx)=>{
            //Verificar si el pedido existe
            const {cliente}=input
            const existePedido = await Pedido.findById(id);
            if(!existePedido){
                throw new Error('El pedido no existe');
            }
            //verificar si el cliente existe
            const existeCliente = await Cliente.findById(cliente);
            if(!existeCliente){
                throw new Error('El cliente no existe');
            }

            //verificar si el cliente y el pedido pertenecen al usuario
            if(existeCliente.vendedor.toString()!==ctx.usuario.id){
                throw new Error('No tienes permisos');
            }
            //revisar stock
            if(input.pedido){
                for await(const articulo of input.pedido){
                    const {id} =articulo;
                    const producto = await Producto.findById(id);
                    if(articulo.cantidad > producto.existencia){
                        throw new Error(`El producto ${producto.nombre} excede la cantidad registrada`)
                    }else{
                        //Restar
                        producto.existencia = producto.existencia - articulo.cantidad
                        await producto.save();
                    }
                }
            }
            //guardar Pedido

            const resultado = await Pedido.findOneAndUpdate({_id:id}, input, {new:true});
            return resultado;

        },
        eliminarPedido:async(_,{id},ctx)=>{
            const existePedido = await Pedido.findById(id);
            if(!existePedido){
                throw new Error('El pedido no existe');
            }
            if(existePedido.vendedor.toString()!== ctx.usuario.id){
                throw new Error('No tienes las credenciales');
            }
            // eliminar de la base de datos
            await Pedido.findOneAndDelete({_id:id});
            return 'Pedido Eliminado';

        }

    }
}
module.exports=resolvers;

